console.log("test");

let trainer = {
    name: "Ash Ketchum",
    Age: 10,
    Pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
    Friends: {
        Hoenn: ['May', 'Max'],
        Kanto: ['Brock', 'Misty'],
    },
    talk: function(){
        console.log(this.Pokemon[0]+" "+"I choose you!");
    }

}
console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer.Pokemon);
trainer.talk();

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;
	
	this.tackle = function(target){
		let newTargetHeatlh = target.health - this.attack;
		if(newTargetHeatlh > 0){
		console.log(this.name +" tackled "+ target.name);
		console.log(target.name+"'s" + " " +"health is now reduced to"+ " "+(newTargetHeatlh));
		console.log(this.Pokemon);
			//console.log(pokemon.target);
	}
		else{
			console.log(this.name +" tackled "+ target.name);
			console.log(target.name+"'s" + " " +"health is now reduced to"+ " "+(newTargetHeatlh));
			console.log(target.name + "has fainted");
			// console.log(pokemon.target);
		} 
	}
	
}

let pikachu = new Pokemon("Pikachu", 12);
	console.log(pikachu);
let Geodude = new Pokemon("Geodude", 8);
	console.log(Geodude);
let Mewtwo = new Pokemon("Mewtwo", 100);
	console.log(Mewtwo);

Geodude.tackle(pikachu);
Mewtwo.tackle(Geodude);

